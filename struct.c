#include<stdio.h>
#include<stdlib.h>
 struct studentData
{
   char studentName[50];
   char subject[50];
   float marks;
};

int main() {
      struct studentData s[5];
      int i;
      for(i=0;i<5;i++)
     {
          printf("\n\n Enter the name of the student : ");
          scanf("%s",&s[i].studentName);
          printf("\n Enter the subject : ");
          scanf("%s",s[i].subject);
          printf("\n Enter marks : ");
          scanf("%f",&s[i].marks);
      }
      printf("\n\n Student Details  ");
      printf("\n-------------------- ");
      for(i=0;i<5;i++)
     {
          printf("\nStudent no.%d", i+1);
          printf("\n--------------");
          printf("\n Student name : %s\n",s[i].studentName);
          printf(" subject : %s \n",s[i].subject);
          printf(" Marks : %.2f\n",s[i].marks);
      }
return 0;
}
